import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router,Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "@fortawesome/fontawesome-free/css/all.css";
import "@fortawesome/fontawesome-free/js/all.js";
import "./App.scss";

import Login from "./components/Login";
import Main from "./components/Main";
import Users from './components/Users';
import ListUsers from './components/ListUsers';
import ListProducts from './components/ListProducts'
import ProviderService from "./services/ProviderServices";
import axios from "axios";

function App() {
    /* FUNCTION POST USERLOGIN */
    const [users, setUsers] = useState();
    const [dataToEdit, setDataToEdit] = useState(null);
    const fetchApi = async () => {
        const response = await fetch('http://127.0.0.1:8000/api/users')
        const responseJSON = await response.json()
       setUsers(responseJSON)
    }
    useEffect(() => {
        fetchApi()
    }, [])
    const loginUser = (datadb) => {
        ProviderService.login(datadb)
        .then(response => {
            setUsers([...users, datadb])
            document.location.href = '/home';
        })
        .catch(error => {
            if (error.response) {
                var t = document.getElementById("error-alert");
                t.classList.toggle("d-none");
                t.innerHTML = "";
                Object.entries(error.response.data.errors).forEach(([key, value]) => {
                    t.insertAdjacentHTML("beforeend", `<li>${value[0]}</li>`);
                });
                setTimeout(() => {
                    var t = document.getElementById("error-alert");
                    t.classList.toggle("d-none");
                }, 2500)
            }
        });
    }
    const create = (datadb) => {
        axios.post('http://localhost:8000/api/create', datadb)
        .then(response => {
            console.log(response);
            setUsers([...users, datadb])
            alert('Se creo el usuario correctamente');
            document.location.href = '/';
        })
        .catch(error =>{

            if (error.response) {
                var t = document.getElementById("error-alert");
                t.classList.toggle("d-none");
                t.innerHTML = "";
                Object.entries(error.response.data.errors).forEach(([key, value]) => {
                    t.insertAdjacentHTML("beforeend", `<li>${value[0]}</li>`);
                });
                setTimeout(() => {
                    var t = document.getElementById("error-alert");
                    t.classList.toggle("d-none");
                }, 2500)
            }
        });
    }

    const update = (datadb) => {
    ProviderService.update(datadb)
        .then(response => {
            let data = response.data;
            let newData = users.map((el) => (el.id === data.id ? data : el));
            setUsers(newData);
        })
        .catch(e => {
            console.log(e);
        });
    }
    /* GET PRODUCTS */
    const [products, setProducts] = useState();
    const fetchProducts = async () => {
        const response = await fetch('http://127.0.0.1:8000/api/products')
        const responseJSON = await response.json()
        setProducts(responseJSON)
    }
    useEffect(() => {
        fetchProducts()
    }, [])
    return (
        <Router>
            <Switch>
                <Route path="/home" exact>
                    <Main/>
                </Route>
                <Route path="/" exact>
                    <Login loginUser={loginUser}/>
                </Route>
                <Route path="/usuarios/:id" component={Users}>
                    <Main/>
                    <Users
                        createData={create}
                        updateData={update}
                        dataToEdit={dataToEdit}
                        setDataToEdit={setDataToEdit}
                        Register = 'true'
                    />
                </Route>
                <Route exact path="/usuarios" component={ListUsers}>
                    <Main/>
                    <ListUsers
                        datadb={users}
                        createData={create}
                        updateData={update}
                        dataToEdit={dataToEdit}
                        setDataToEdit={setDataToEdit}
                    />
               </Route>
                <Route exact path="/productos" component={ListProducts}>
                    <Main/>
                    <ListProducts
                        datadb={products}
                    />
                </Route>
                <Route path="/add">
                    <Main/>
                    <Users
                        createData={create}
                        updateData={update}
                    />
                </Route>
                <Route path="/register">
                    <Users
                        createData={create}
                        updateData={update}
                        dataToEdit={dataToEdit}
                        setDataToEdit={setDataToEdit}
                        Register = 'false'
                    />
                </Route>
            </Switch>
        </Router>
    );

}

export default App;
