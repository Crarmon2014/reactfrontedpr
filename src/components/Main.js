import React from "react";
import {  Link } from "react-router-dom";
const Main = () => {
    return (
        <div>
            <nav className="navbar navbar-expand navbar-dark bg-dark">
                <a href="/home" className="navbar-brand">
                    SOFTSOLUTION
                </a>
                <div className="navbar-nav mr-auto">
                    <li className="nav-item">
                        <Link to={"/usuarios"} className="nav-link">
                            Usuarios
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link to={"/productos"} className="nav-link">
                            Productos
                        </Link>
                    </li>
                    <li className="nav-item">
                        <Link to={"/add"} className="nav-link">
                            Agregar Usuario
                        </Link>
                    </li>
                </div>
            </nav>
        </div>
    );
}

export default Main;
