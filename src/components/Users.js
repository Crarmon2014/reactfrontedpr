import React from "react";
import RegisterForm from "./RegisterForm"

const Users = ({ createData, updateData, dataToEdit, setDataToEdit,Register }) => {
    return (
        <div class="container">
            <div className="row">
                <div className="col">
                    <div>
                        <hr/>
                        <div className="btn-dark mb-3">
                            <h1 className="display-4 text-center">Registro de Usuario</h1>
                        </div>
                        <div className="row">
                            <div className="col">
                                <RegisterForm
                                    createData={createData}
                                    updateData={updateData}
                                    dataToEdit={dataToEdit}
                                    setDataToEdit={setDataToEdit}
                                    Register={Register}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Users;