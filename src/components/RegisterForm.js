import React, { useState, useEffect } from "react";
import {Link} from "react-router-dom";

const initialForm = {
    name: '',
    email: '',
    password: '',
    telf: '',
    cpf: '',
    fecha_nac: '',
}

const RegisterForm = ({ createData, updateData, dataToEdit, setDataToEdit,Register }) => {

    const [values, setValues] = useState(initialForm)

    useEffect(() => {
        if (dataToEdit) {
            setValues(dataToEdit);
        } else {
            setValues(initialForm);
        }
    }, [dataToEdit]);

    const handleChange = (e) => {
        setValues({
            ...values,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        if (!values.name
            || !values.email
            || !values.password
            || !values.telf
            || !values.cpf
            || !values.fecha_nac
        ) {
            alert("Datos incompletos");
            return;
        }
        if (values.id == null) {
            createData(values);
        } else {
            updateData(values);
        }

        handleReset();
    };

    const handleReset = (e) => {
        setValues(initialForm);
        setDataToEdit(null);
    };

    return (

        <div className="row">
            <div className="col">
                <div className="alert alert-danger d-none" role="alert" id="error-alert">
                </div>
                <div className="alert alert-primary d-none" role="alert" id="success-alert">
                </div>
                <form autoComplete="off" onSubmit={handleSubmit}>

                    <div className="form-row mb-3 input-group">
                        <div className="text-dark mt-2 col-2 text-center">
                            <i className="material-icons ml-5">Nombres:</i>
                        </div>
                        <input className="form-control" placeholder="Introduzca Nombres" name="name" onChange={handleChange}
                               value={values.name}
                        />
                    </div>

                    <div className="form-row mb-3 input-group">
                        <div className="text-dark mt-2 col-2 text-center">
                            <i className="material-icons ml-5">Email:</i>
                        </div>
                        <div className="form-group col 10">
                            <input className="form-control" type="email" placeholder="Email" name="email" onChange={handleChange}
                                   value={values.email}
                            />
                        </div>
                    </div>

                    <div className="form-row mb-3 input-group">
                        <div className="text-dark mt-2 col-2 text-center">
                            <i className="material-icons ml-5">Contraseña:</i>
                        </div>
                        <div className="form-group col 10">
                            <input className="form-control" type="password" placeholder="Introduzca Contraseña" name="password" onChange={handleChange}
                                   value={values.password}
                            />
                        </div>
                    </div>

                    <div className="form-row mb-3 input-group">
                        <div className="text-dark mt-2 col-2 text-center">
                            <i className="material-icons ml-5">Telefono:</i>
                        </div>
                        <div className="form-group col 10">
                            <input className="form-control" type="text" placeholder="Introduzca telefono" name="telf" onChange={handleChange}
                                   value={values.telf}
                            />
                        </div>
                    </div>

                    <div className="form-row mb-3 input-group">
                        <div className="text-dark mt-2 col-2 text-center">
                            <i className="material-icons ml-5">FPF:</i>
                        </div>
                        <div className="form-group col 10">
                            <input className="form-control" type="text" placeholder="CPF" name="cpf" onChange={handleChange}
                                   value={values.cpf}
                            />
                        </div>
                    </div>

                    <div className="form-row mb-3 input-group">
                        <div className="text-dark mt-2 col-2 text-center">
                            <i className="material-icons ml-5">Fecha Nacimiento:</i>
                        </div>
                        <div className="form-group col 10">
                            <input className="form-control" type="date" name="fecha_nac" onChange={handleChange}
                                   value={values.fecha_nac}
                            />
                        </div>
                    </div>
                    <div className="container mb-3">

                        <div className="row-cols-md-6" style={{position: "relative",left:"480px"}}>
                            <button type="submit" id="btnSubmit" className="btn btn-primary btn">Registrar</button>
                            {Register
                                ? <Link className="btn btn-danger" to={"/"}>Cancelar</Link>
                                : ''
                            }
                        </div>
                    </div>
                </form >
            </div>
        </div>

    );
}

export default RegisterForm;