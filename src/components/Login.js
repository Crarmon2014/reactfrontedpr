import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const userState  = { email: '',  password: ''}
const errorState = { error:false}
const Login = ({ loginUser }) => {
    const [user, setUser]   = useState(userState)
    const [error, setError] = useState(errorState)

    const handleChange = (e) => {
        setUser({
            ...user,
            [e.target.name]: e.target.value,
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (!user.email || !user.password) {
            setError({           ...error,error:true});
        }
        loginUser(user);
    }
    return (
    <div class="container">
        <div className="wrapper">
            <div className="rec-prism">
                <div className="face face-top">
                    <div className="content">
                        <form onSubmit="event.preventDefault()">
                            <div className="field-wrapper">
                                <input className="form-control" type="email" placeholder="Email" name="email"
                                       value={user.email}  onChange={handleChange}
                                />
                            </div>
                            <div className="field-wrapper">
                                <input type="submit" />
                            </div>
                        </form>
                    </div>
                </div>
                <div className="face face-front">
                    <div className="content">
                        <h2>Iniciar Sesion</h2>
                        <div className="alert alert-danger d-none" role="alert" id="error-alert">
                        </div>
                        <form onSubmit={handleSubmit}>
                            <div className="field-wrapper">
                                <input type="text" name="email" placeholder="Email"  value={user.email}  onChange={handleChange}/>
                                <label>Usuario</label>
                            </div>
                            <div className="field-wrapper">
                                <input type="password" name="password" placeholder="password"
                                       autoComplete="new-password"  value={user.password}  onChange={handleChange}/>
                                    <label>password</label>
                            </div>
                            <div className="field-wrapper">
                                <input type="submit" value="Entrar"/>
                            </div>
                            <br/>
                            <div className="row">
                                <div className="col">
                                    <Link to="/register" className="btn btn-primary form-control btn-block">Registrar</Link>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    );
}

export default Login;