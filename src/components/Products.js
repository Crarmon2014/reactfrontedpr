import React from "react";
import ListProducts from "./ProductsTable"

const Products = ({ datadb }) => {
    return (
        <div>
            <div className="card bg-info text-white mb-3">
                <h1 className="display-4 text-center">Pro</h1>
            </div>

            <div className="row">
                <div className="col">
                    <ListProducts datadb = {datadb}/>
                </div>
            </div>
        </div>
    );
}

export default Products;