import React from "react";
import ListRowUser from "./ListUsersRow";

const ListUsers = ({ datadb, setDataToEdit }) => {
    return (
        <div class="container">
            <hr/>
            <span className="pull-left"><h3>Listado de Usuarios</h3></span>
            <div className="row">
                <div className="col">
                    <div>
                        <table className="table table-stripped table-hover">
                            <thead className="thead-dark">
                            <tr>
                                <th>Id</th>
                                <th>Nombres</th>
                                <th>Correo</th>
                                <th>Telefono</th>
                                <th>CPF</th>
                                <th>Fecha de Cumpleaños</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {datadb.length > 0 ? (
                                datadb.map((el) => (
                                    <ListRowUser
                                        key={el.id}
                                        el={el}
                                        setDataToEdit={setDataToEdit}
                                    />
                                ))
                            ) : (
                                <tr>
                                    <td colSpan="4">Registro no encontrado...</td>
                                </tr>
                            )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ListUsers;