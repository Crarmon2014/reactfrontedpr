import React from "react";

const ListProducts = ({datadb}) => {
    return (
        <div className="container">
            <hr/>
            <span className="pull-left"><h3>Listado de Productos</h3></span>
            <div className="row">
                <div className="col">
                <table className="table table-stripped table-hover">
                    <thead className="thead-dark">
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Serie</th>
                        <th>Length</th>
                        <th>Height</th>
                    </tr>
                    </thead>
                    <tbody>
                    { datadb.length > 0 ? (
                        datadb.map((el) => (
                            <tr>
                                <td>{el.id}</td>
                                <td>{el.name}</td>
                                <td>{el.serie}</td>
                                <td>{el.length}</td>
                                <td>{el.height}</td>
                            </tr>
                        ))
                    ) : (
                        <tr>
                            <td colSpan="5">Records not found...</td>
                        </tr>
                    )}
                    </tbody>
                </table>
              </div>
            </div>
        </div>
    );
}

export default ListProducts;