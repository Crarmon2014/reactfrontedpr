import React from "react";
import {  Link } from "react-router-dom";

const ListUsersRow = ({el, setDataToEdit}) => {
    let { id, name, email, telf, cpf,fecha_nac } = el;
    return (
        <tr>
            <td>{id}</td>
            <td>{name}</td>
            <td>{email}</td>
            <td>{telf}</td>
            <td>{cpf}</td>
            <td>{fecha_nac}</td>
        </tr>
    );
}


export default ListUsersRow;