import http from "../http-common";

const login = (data) => {
  return http.post("http://localhost:8000/api/login", data);
};

const create = (data) => {
  return http.post(`http://localhost:8000/api/register`, data);
};

const update = (data) => {
  return http.post(`http://localhost:8000/api/user/update`, data);
};


const ProviderService = {
  login,
  create,
  update
};

export default ProviderService;
